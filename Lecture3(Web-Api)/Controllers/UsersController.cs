﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture3.Services.Interfaces;
using Lecture3.Services.DTOs;
using Lecture3.Services.AuxiliaryModels;
using Lecture3.Services.Models;
using AutoMapper;

namespace Lecture3_Web_Api_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _repository;
        private readonly IMapper _mapper;

        public UsersController(IRepository<User> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<UserDTO>> GetUsers()
        {
            List<User> users;
            try
            {
                users = _repository.GetAll().ToList();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            List<UserDTO> userDTOs = _mapper.Map<List<UserDTO>>(users);

            return Ok(userDTOs);
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            User user;
            try
            {
                user = _repository.GetById(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            UserDTO userDto = _mapper.Map<UserDTO>(user);

            return Ok(userDto);
        }

        [HttpPost]
        public ActionResult Post([FromBody] UserDTO userDTO)
        {
            if (userDTO == null)
                return BadRequest();

            if (userDTO.Id < 0)
                return BadRequest("Wrong request body");

            User user = _mapper.Map<User>(userDTO);

            try
            {
                _repository.Add(user);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", userDTO);
        }

        [HttpPut]
        public ActionResult Put([FromBody] UserDTO userDTO)
        {
            if (userDTO == null)
                return BadRequest();

            if (userDTO.Id < 0)
                return BadRequest("Wrong request body");

            User user = _mapper.Map<User>(userDTO);

            try
            {
                _repository.Update(user);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
