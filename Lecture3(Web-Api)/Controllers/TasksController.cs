﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Lecture3.Services.Interfaces;
using Lecture3.Services.DTOs;
using Lecture3.Services.AuxiliaryModels;
using Lecture3.Services.Models;
using AutoMapper;

namespace Lecture3_Web_Api_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly IRepository<Task> _repository;
        private readonly IMapper _mapper;

        public TasksController(IRepository<Task> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDTO>> GetTeams()
        {
            List<Task> tasks;
            try
            {
                tasks = _repository.GetAll().ToList();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            List<TaskDTO> taskDTOs = _mapper.Map<List<TaskDTO>>(tasks);

            return Ok(taskDTOs);
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            Task task;
            try
            {
                task = _repository.GetById(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            TaskDTO taskDTO = _mapper.Map<TaskDTO>(task);

            return Ok(taskDTO);
        }

        [HttpPost]
        public ActionResult Post([FromBody] TaskDTO taskDTO)
        {
            if (taskDTO == null)
                return BadRequest();

            if (taskDTO.Id < 0)
                return BadRequest("Wrong request body");

            Task task = _mapper.Map<Task>(taskDTO);

            try
            {
                _repository.Add(task);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", taskDTO);
        }

        [HttpPut]
        public ActionResult Put([FromBody] TaskDTO taskDTO)
        {
            if (taskDTO == null)
                return BadRequest();

            if (taskDTO.Id < 0)
                return BadRequest("Wrong request body");

            Task task = _mapper.Map<Task>(taskDTO);

            try
            {
                _repository.Update(task);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
