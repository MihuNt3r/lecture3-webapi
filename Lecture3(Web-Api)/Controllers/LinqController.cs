﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Lecture3.Services.Interfaces;
using Lecture3.Services.DTOs;
using Lecture3.Services.AuxiliaryModels;
using Lecture3.Services.Models;
using Lecture3.Services.Services;
using AutoMapper;

namespace Lecture3_Web_Api_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly IRepository<Project> _projectRepository;
        private readonly IRepository<Team> _teamRepository;
        private readonly IRepository<Task> _taskRepository;
        private readonly IRepository<User> _userRepository;

        private readonly IMapper _mapper;
        private readonly JoinService _service;

        public LinqController(IRepository<Project> projectRepository, IRepository<Team> teamRepository,
                              IRepository<Task> taskRepository, IRepository<User> userRepository,
                              IMapper mapper, JoinService service)
        {
            _projectRepository = projectRepository;
            _teamRepository = teamRepository;
            _taskRepository = taskRepository;
            _userRepository = userRepository;
            _mapper = mapper;
            _service = service;
        }

        [HttpGet("1sttask/{id}")]
        public ActionResult<Dictionary<ProjectDTO, int>> GetTasksAmountByUserId(int id)
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from t in tasks
                         where t.Performer.Id == id
                         select new
                        {
                            Proj = _mapper.Map<ProjectDTO>(t.Project),
                            TasksCount = t.Project.Tasks.Count
                        }).Distinct().ToDictionary(t => t.Proj, t => t.TasksCount);;

            return query;
        }

        [HttpGet("2ndtask/{id}")]
        public ActionResult<IEnumerable<TaskDTO>> GetListOfTasksByUserId(int id)
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from t in tasks
                         where t.Performer.Id == id && t.Name.Length < 45
                         select _mapper.Map<TaskDTO>(t)).ToList();

            return query;
        }

        [HttpGet("3rdtask/{id}")]
        public ActionResult<IEnumerable<Task3Model>> GetTasksFinishedIn2021(int id)
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from t in tasks
                         where t.PerformerId == id
                         where t.FinishedAt?.Year == 2021
                         select new Task3Model
                         {
                             Id = t.Id,
                             Name = t.Name
                         }).ToList();

            return query;
        }

        [HttpGet("4thtask")]
        public ActionResult<IEnumerable<Task4Model>> Task4Method()
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from t in teams
                         select new Task4Model
                         {
                             Id = t.Id,
                             Name = t.Name,
                             Users = (from u in users
                                      where u.BirthDay.Year < (DateTime.Now.Year - 10) &&
                                      u.TeamId == t.Id
                                      orderby u.RegisteredAt descending
                                      select u).ToList()
                                      
                         }).Distinct().ToList();

            return query;
        }

        [HttpGet("5thtask")]
        public ActionResult<IEnumerable<UserDTO>> GetSortedUsers()
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from u in users
                         orderby u.FirstName
                         select _mapper.Map<UserDTO>(u)).ToList();

            return query;
        }

        [HttpGet("6thtask/{id}")]
        public ActionResult<Task6Model> Task6Method(int id)
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from u in users
                         where u.Id == id
                         select new Task6Model
                         {
                             User = u,
                             LastProject = u.Tasks.Aggregate((curLast, x) => curLast.CreatedAt < x.CreatedAt ? x : curLast).Project,
                             LastProjectTasks = u.Tasks.Aggregate((curLast, x) => curLast.CreatedAt < x.CreatedAt ? x : curLast).Project.Tasks.Count,
                             NotFinishedTasksCount = (from t in u.Tasks
                                                     where t.FinishedAt == null
                                                     select t).ToList().Count,
                             TaskWithLongestPeriod = u.Tasks.Aggregate((curLongst, x) => 
                            (((curLongst.FinishedAt ?? DateTime.Now) - curLongst.CreatedAt) < ((x.FinishedAt ?? DateTime.Now) - x.CreatedAt)) ? 
                            x : curLongst)
                         }).FirstOrDefault();
            return query;
        }

        [HttpGet("7thtask")]
        public ActionResult<IEnumerable<Task7Model>> Task7Method()
        {
            var tasks = _taskRepository.GetAll().ToList();
            var users = _userRepository.GetAll().ToList();
            var projects = _projectRepository.GetAll().ToList();
            var teams = _teamRepository.GetAll().ToList();

            _service.JoinEntities(teams, users, projects, tasks);

            var query = (from p in projects
                         select new Task7Model
                         {
                             Project = p,
                             LongestTaskByDescription = p.Tasks.Count > 0 ?
                             p.Tasks.Aggregate((curMax, x) => curMax.Description.Length < x.Description.Length ? x : curMax) : null,

                             ShortestTaskByName = p.Tasks.Count > 0 ?
                             p.Tasks.Aggregate((curMin, x) => curMin.Name.Length > x.Name.Length ? x : curMin) : null,

                             CustomersCount = p.Description.Length > 20 || p.Tasks.Count < 3 ?
                             p.Team.Customers.Count : 0
                         }).ToList();

            return query;
        }
    }
}
