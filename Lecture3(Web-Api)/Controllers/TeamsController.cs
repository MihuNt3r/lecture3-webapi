﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture3.Services.Interfaces;
using Lecture3.Services.DTOs;
using Lecture3.Services.AuxiliaryModels;
using Lecture3.Services.Models;
using AutoMapper;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Lecture3_Web_Api_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly IRepository<Team> _repository;
        private readonly IMapper _mapper;

        public TeamsController(IRepository<Team> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDTO>> GetTeams()
        {
            List<Team> teams;
            try
            {
                teams = _repository.GetAll().ToList();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            List<TeamDTO> teamDTOs = _mapper.Map<List<TeamDTO>>(teams);

            return Ok(teamDTOs);
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            Team team;
            try
            {
                team = _repository.GetById(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            TeamDTO teamDTO = _mapper.Map<TeamDTO>(team);

            return Ok(teamDTO);
        }

        [HttpPost]
        public ActionResult Post([FromBody] TeamDTO teamDTO)
        {
            if (teamDTO == null)
                return BadRequest();

            if (teamDTO.Id < 0)
                return BadRequest("Wrong request body");

            Team team = _mapper.Map<Team>(teamDTO);

            try
            {
                _repository.Add(team);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", teamDTO);
        }

        [HttpPut]
        public ActionResult Put([FromBody] TeamDTO teamDto)
        {
            if (teamDto == null)
                return BadRequest();

            if (teamDto.Id < 0)
                return BadRequest("Wrong request body");

            Team team = _mapper.Map<Team>(teamDto);

            try
            {
                _repository.Update(team);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
