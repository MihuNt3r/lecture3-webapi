﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Lecture3.Services.Interfaces;
using Lecture3.Services.DTOs;
using Lecture3.Services.AuxiliaryModels;
using Lecture3.Services.Models;
using AutoMapper;

namespace Lecture3_Web_Api_.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IRepository<Project> _repository;
        private readonly IMapper _mapper;

        public ProjectsController(IRepository<Project> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDTO>> GetProjects()
        {
            List<Project> projects;
            try
            {
                projects = _repository.GetAll().ToList();
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            List<ProjectDTO> projectDTOs = _mapper.Map<List<ProjectDTO>>(projects);

            return Ok(projectDTOs);
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            Project project;
            try
            {
                project = _repository.GetById(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            ProjectDTO projectDto = _mapper.Map<ProjectDTO>(project);

            return Ok(projectDto);
        }

        [HttpPost]
        public ActionResult Post([FromBody] ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                return BadRequest();

            if (projectDTO.Id < 0)
                return BadRequest("Wrong request body");

            Project project = _mapper.Map<Project>(projectDTO);

            try
            {
                _repository.Add(project);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created("", projectDTO);
        }

        [HttpPut]
        public ActionResult Put([FromBody] ProjectDTO projectDTO)
        {
            if (projectDTO == null)
                return BadRequest();

            if (projectDTO.Id < 0)
                return BadRequest("Wrong request body");

            Project project = _mapper.Map<Project>(projectDTO);

            try
            {
                _repository.Update(project);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _repository.Delete(id);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }

            return NoContent();
        }
    }
}
