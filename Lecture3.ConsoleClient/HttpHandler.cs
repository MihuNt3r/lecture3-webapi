﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Json;
using System.Net.Http.Headers;
using Lecture3.Services.DTOs;
using Lecture3.Services.AuxiliaryModels;

namespace Lecture3.ConsoleClient
{
    public class HttpHandler
    {
        private readonly HttpClient _client;

        public HttpHandler()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:44366/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<ProjectDTO>> GetProjects()
        {
            var action = "api/projects";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var projects = await response.Content.ReadFromJsonAsync<List<ProjectDTO>>();
                return projects;
            }

            throw new Exception("Not found");
        }
        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var action = $"api/projects/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var project = await response.Content.ReadFromJsonAsync<ProjectDTO>();
                return project;
            }

            throw new Exception("Not found");
        }

        public async Task<List<TaskDTO>> GetTasks()
        {
            var action = "api/tasks";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var tasks = await response.Content.ReadFromJsonAsync<List<TaskDTO>>();
                return tasks;
            }

            throw new Exception("Not found");
        }

        public async Task<TaskDTO> GetTaskById(int id)
        {
            var action = $"api/tasks/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var task = await response.Content.ReadFromJsonAsync<TaskDTO>();
                return task;
            }

            throw new Exception("Not found");
        }

        public async Task<List<TeamDTO>> GetTeams()
        {
            var action = "api/teams";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var teams = await response.Content.ReadFromJsonAsync<List<TeamDTO>>();
                return teams;
            }

            throw new Exception("Not found");
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var action = $"api/teams/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var team = await response.Content.ReadFromJsonAsync<TeamDTO>();
                return team;
            }

            throw new Exception("Not found");
        }

        public async Task<List<UserDTO>> GetUsers()
        {
            var action = "api/users";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var users = await response.Content.ReadFromJsonAsync<List<UserDTO>>();
                return users;
            }

            throw new Exception("Not found");
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var action = $"api/users/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var user = await response.Content.ReadFromJsonAsync<UserDTO>();
                return user;
            }

            throw new Exception("Not found");
        }

        public async Task<Dictionary<ProjectDTO, int>> GetDictionary(int id)
        {
            var action = $"api/linq/1sttask/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var dict = await response.Content.ReadFromJsonAsync<Dictionary<ProjectDTO, int>>();
                return dict;
            }

            throw new Exception("Not found");
        }

        public async Task<List<TaskDTO>> GetListOfTasksByUserId(int id)
        {
            var action = $"api/linq/2ndtask/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<TaskDTO>>();
                return list;
            }

            throw new Exception("Not found");
        }

        public async Task<List<Task3Model>> GetTasksFinishedIn2021(int id)
        {
            var action = $"api/linq/3rdtask/{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<Task3Model>>();
                return list;
            }

            throw new Exception("Not found");
        }

        public async Task<List<Task4Model>> Task4Method()
        {
            var action = $"api/linq/4thtask";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<Task4Model>>();
                return list;
            }

            throw new Exception("Not found");
        }

        public async Task<List<UserDTO>> GetSortedUsers()
        {
            var action = $"api/linq/5thtask";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<UserDTO>>();
                return list;
            }

            throw new Exception("Not found");
        }

        public async Task<Task6Model> Task6Method(int id)
        {
            var action = $"api/linq/6thtask{id}";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<Task6Model>();
                return list;
            }

            throw new Exception("Not found");
        }

        public async Task<List<Task7Model>> Task7Method()
        {
            var action = $"api/linq/7thtask";

            HttpResponseMessage response = await _client.GetAsync(action);
            if (response.IsSuccessStatusCode)
            {
                var list = await response.Content.ReadFromJsonAsync<List<Task7Model>>();
                return list;
            }

            throw new Exception("Not found");
        }

    }
}
