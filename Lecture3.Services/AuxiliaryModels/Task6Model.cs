﻿using Lecture3.Services.Models;

namespace Lecture3.Services.AuxiliaryModels
{
    public class Task6Model
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int LastProjectTasks { get; set; }
        public int NotFinishedTasksCount { get; set; }
        public Task TaskWithLongestPeriod { get; set; }
    }
}
