﻿using System.Collections.Generic;
using Lecture3.Services.Models;

namespace Lecture3.Services.AuxiliaryModels
{
    public class Task4Model
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; }
    }
}
