﻿using Lecture3.Services.Models;

namespace Lecture3.Services.AuxiliaryModels
{
    public class Task7Model
    {
        public Project Project { get; set; }
        public Task LongestTaskByDescription { get; set; }
        public Task ShortestTaskByName { get; set; }
        public int CustomersCount { get; set; }
    }
}
