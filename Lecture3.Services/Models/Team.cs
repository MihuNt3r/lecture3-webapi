﻿using System;
using System.Collections.Generic;
using Lecture3.Services.Interfaces;

namespace Lecture3.Services.Models
{
    public class Team : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Customers {get; set;}
        public Team()
        {
            Customers = new List<User>();
        }
    }
}
