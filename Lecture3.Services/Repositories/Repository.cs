﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lecture3.Services.Interfaces;

namespace Lecture3.Services.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {
        protected readonly List<TEntity> _list;

        public Repository()
        {
            _list = new List<TEntity>();
        }

        public void Add(TEntity entity)
        {
            var duplicate = _list.Find(e => e.Id == entity.Id);

            if (duplicate != null)
                throw new ArgumentException("Entity with such Id already exists");

            _list.Add(entity);
        }

        public void Delete(int id)
        {
            var entityToDelete = _list.Find(e => e.Id == id);

            if (entityToDelete == null)
                throw new ArgumentException("Can't find entity with such id");

            _list.Remove(entityToDelete);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _list;
        }

        public TEntity GetById(int id)
        {
            var entity = _list.Find(e => e.Id == id);

            if (entity == null)
                throw new ArgumentException("Can't find entity with such id");

            return entity;
        }

        public void Update(TEntity entity)
        {
            int index = _list.FindIndex(e => e.Id == entity.Id);

            if (index == -1)
                throw new ArgumentException("Can't find entity with such id");

            _list.RemoveAt(index);
            _list.Add(entity);
        }
    }
}
