﻿using System.Collections.Generic;
using System.Linq;
using Lecture3.Services.Models;

namespace Lecture3.Services.Services
{
    public class JoinService
    {
        public void JoinEntities(List<Team> teams, List<User> users,
                                 List<Project> projects, List<Task> tasks)
        {
            users = (from u in users
                     join t in teams
                     on u.TeamId equals t.Id
                     select new User
                     {
                         Id = u.Id,
                         TeamId = u.TeamId,
                         Team = t,
                         FirstName = u.FirstName,
                         LastName = u.LastName,
                         Email = u.Email,
                         RegisteredAt = u.RegisteredAt,
                         BirthDay = u.BirthDay
                     }).ToList();


            projects = (from p in projects
                        join t in teams
                        on p.TeamId equals t.Id
                        join u in users
                        on p.AuthorId equals u.Id
                        select new Project
                        {
                            Id = p.Id,
                            AuthorId = p.AuthorId,
                            Author = u,
                            TeamId = p.TeamId,
                            Team = t,
                            Name = p.Name,
                            Description = p.Description,
                            Deadline = p.Deadline
                        }).ToList();

            tasks = (from t in tasks
                     join p in projects
                     on t.ProjectId equals p.Id
                     join u in users
                     on t.PerformerId equals u.Id
                     select new Task
                     {
                         Id = t.Id,
                         ProjectId = t.ProjectId,
                         Project = p,
                         PerformerId = t.PerformerId,
                         Performer = u,
                         Name = t.Name,
                         Description = t.Description,
                         State = t.State,
                         CreatedAt = t.CreatedAt,
                         FinishedAt = t.FinishedAt
                     }).ToList();

            tasks.ForEach(t => t.Performer.Tasks.Add(t));
            tasks.ForEach(t => t.Project.Tasks.Add(t));
            users.ForEach(u => u.Team.Customers.Add(u));
        }
    }
}
