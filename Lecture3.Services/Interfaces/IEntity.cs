﻿namespace Lecture3.Services.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
