﻿using System.Collections.Generic;

namespace Lecture3.Services.Interfaces
{
    public interface IRepository<TEntity> where TEntity : IEntity
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        void Add(TEntity entity);
        void Delete(int id);
        void Update(TEntity entity);
    }
}
